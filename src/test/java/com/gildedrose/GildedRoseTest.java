package com.gildedrose;

import static org.junit.Assert.assertEquals;

import java.util.stream.IntStream;

import org.junit.Test;

public class GildedRoseTest {

    @Test
    public void usuallyItemDecrementation() {
        Item[] items = new Item[] { new Item("+5 Dexterity Vest", 10, 20) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(9, app.items[0].sellIn);
        assertEquals(19, app.items[0].quality);
    }
    
    @Test
    public void usuallyItemDecrementationInDias() {
        Item[] items = new Item[] { new Item("+5 Dexterity Vest", 10, 20) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 3;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(7, app.items[0].sellIn);
        assertEquals(17, app.items[0].quality);
    }
    
    @Test
    public void usuallyItemDecrementationInDiasQuality0() {
        Item[] items = new Item[] { new Item("+5 Dexterity Vest", 10, 20) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 50;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(-40, app.items[0].sellIn);
        assertEquals(0, app.items[0].quality);
    }
    
    @Test
    public void sufuraItemSameDatas() {
        Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 0, 80) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 50;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(0, app.items[0].sellIn);
        assertEquals(80, app.items[0].quality);
    }
    
    @Test
    public void brieItemIncreaseQualityPlus1() {
        Item[] items = new Item[] { new Item("Aged Brie", 2, 0) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 2;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(0, app.items[0].sellIn);
        assertEquals(2, app.items[0].quality);
    }
    
    @Test
    public void brieItemIncreaseQualityPlus2() {
        Item[] items = new Item[] { new Item("Aged Brie", 2, 0) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 4;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(-2, app.items[0].sellIn);
        assertEquals(6, app.items[0].quality);
    }
    
    @Test
    public void brieItemIncreaseQualityMax50() {
        Item[] items = new Item[] { new Item("Aged Brie", 2, 0) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 50;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(-48, app.items[0].sellIn);
        assertEquals(50, app.items[0].quality);
    }
    
    @Test
    public void backstageItemInSellMore10() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 5;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(10, app.items[0].sellIn);
        assertEquals(25, app.items[0].quality);
    }
    
    @Test
    public void backstageItemInSellLess10() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 10, 20) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 5;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(5, app.items[0].sellIn);
        assertEquals(30, app.items[0].quality);
    }
    
    @Test
    public void backstageItemInSellLess5() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 20) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 5;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(0, app.items[0].sellIn);
        assertEquals(35, app.items[0].quality);
    }
    
    @Test
    public void backstageItemIncreaseQuality0() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 20) };
        GildedRose app = new GildedRose(items);
        int startDay = 0;
        int endDay = 50;
        
        IntStream.range(startDay, endDay).forEach(item -> app.updateQuality());

        assertEquals(-45, app.items[0].sellIn);
        assertEquals(0, app.items[0].quality);
    }
    

}
