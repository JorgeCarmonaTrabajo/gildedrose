package com.gildedrose;

import java.util.Arrays;

class GildedRose {
	//sellIn que denota el número de días que tenemos para venderlo
	//quality que denota cúan valioso es el artículo
    Item[] items;
    
    public static final String TYPE_BRIE = "brie";
    public static final String TYPE_SULFURAS = "sulfuras";
    public static final String TYPE_BACKSTAGE = "backstage";
    public static final String TYPE_CONJURADOS = "conjurados";

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
    	Arrays.stream(items).forEach(item -> dispatcherUpdateQuality(item));
    }
    
    //Dependiendo del tipo de item haremos un tratamiento u otro
    private void dispatcherUpdateQuality(Item item) {
    	if(item.name.toLowerCase().contains(TYPE_BRIE))
    		ItemStrategy.brieItemStrategy(item);
    	else if(item.name.toLowerCase().contains(TYPE_SULFURAS))
    		ItemStrategy.sufuraItemStrategy(item);
    	else if(item.name.toLowerCase().contains(TYPE_BACKSTAGE))
    		ItemStrategy.backstageItemStrategy(item);
    	else if(item.name.toLowerCase().contains(TYPE_CONJURADOS))
    		ItemStrategy.conjuradosItemStrategy(item);    	
    	else
    		ItemStrategy.usuallyItemStrategy(item);
    	
    }
}