package com.gildedrose;

public interface ItemStrategy {

	public static final int MIN_QUALITY = 0;
	public static final int MAN_QUALITY = 50;
	
	static void usuallyItemStrategy(Item item) {
		item.sellIn -= 1;
		item.quality -= 1;
		item.quality = (item.quality < MIN_QUALITY)?MIN_QUALITY:item.quality;
	}
	
	//Su calidad aumenta en 1 unidad cada día
	//luego de la fecha de venta su calidad aumenta 2 unidades por día
	static void brieItemStrategy(Item item) {
		if(item.sellIn > 0) {
			item.quality += 1;
		} else {
			item.quality += 2;
		}
		item.sellIn -= 1;
		item.quality = (item.quality > MAN_QUALITY)?MAN_QUALITY:item.quality;
	}
	
	//no modifica su fecha de venta ni se degrada en calidad
	static void sufuraItemStrategy(Item item) {
		//mantenemos los valores
	}
	
	//si faltan 10 días o menos para el concierto, la calidad se incrementa en 2 unidades
	//si faltan 5 días o menos, la calidad se incrementa en 3 unidades
	//luego de la fecha de venta la calidad cae a 0
	static void backstageItemStrategy(Item item) {
		if(item.sellIn <= 0) {
			item.quality = 0;	
		} else if(item.sellIn > 10) {
			item.quality += 1;
		} else if(item.sellIn <= 5) {
			item.quality += 3;
		} else if(item.sellIn <= 10) {
			item.quality += 2;
		}
		item.sellIn -= 1;
		item.quality = (item.quality < MIN_QUALITY)?MIN_QUALITY:item.quality;
		item.quality = (item.quality > MAN_QUALITY)?MAN_QUALITY:item.quality;
	}
	
	//Los artículos conjurados degradan su calidad al doble de velocidad que los normales
	static void conjuradosItemStrategy(Item item) {
		item.sellIn -= 1;
		item.quality -= 2;
		item.quality = (item.quality < MIN_QUALITY)?MIN_QUALITY:item.quality;
	}
}
